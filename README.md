# My Project

# Install

```
python -m venv "venv"  # create virtual environment named "venv" to ensure the project dependencies are sand-boxed.
source ./venv/bin/activate  # activate virtualenv. Note that there are "activate" scripts for other shells
# Note: It's recommended to have pip updated whenever possible, for that, run: python -m pip install --upgrade pip
python -m pip install -r requirements.txt  # install dependencies
```

